extends CanvasLayer


func show_message(text):
    $Message.text = text
    $Message.show()
    $MessageTimer.start()


func show_game_over():
    show_message("Game Over")
    await $MessageTimer.timeout

    $Message.text = "Dodge the mob!!!"
    $Message.show()

    await get_tree().create_timer(1.0).timeout


func update_level(level):
    $LevelLabel.text = str(level)


func update_score(score):
    $ScoreLabel.text = str(score)


func update_high_score(high_score):
    $HighScoreLabel.text = str(high_score)


func _ready():
    pass


func _process(delta):
    pass


func _on_message_timer_timeout():
    $Message.hide()
