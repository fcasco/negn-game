extends Node


@export var mob_scene: PackedScene


var high_score
var level
var score


func _ready():
    high_score = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    pass


func new_game():
    $MainMenu.hide()

    score = 0
    $HUD.update_score(score)

    level = 1
    $HUD.update_level(level)

    $HUD.show_message("Get Ready")

    $Player.start($StartPosition.position)
    $Music.play()
    $StartTimer.start()


func game_over():
    get_tree().call_group("mobs", "queue_free")

    $Music.stop()
    $DeathSound.play()
    $LevelTimer.stop()
    $MobTimer.stop()
    $HUD.show_game_over()
    if score > high_score:
        high_score = score
        $HUD.update_high_score(score)

    $MainMenu.show()


func quit_game():
    get_tree().quit()


func _on_start_timer_timeout():
    $MobTimer.start()
    $LevelTimer.start()


func _on_level_timer_timeout():
    level += 1
    $HUD.update_level(level)


func _on_bug_missed():
    score += 1
    $HUD.update_score(score)


func _on_mob_timer_timeout():
    for n in level:
        var mob = mob_scene.instantiate()
        mob.connect("bug_missed", _on_bug_missed)

        var mob_spawn_location = get_node("MobPath/MobSpawnLocation")
        mob_spawn_location.progress_ratio = randf()

        var direction = mob_spawn_location.rotation + PI / 2

        mob.position = mob_spawn_location.position

        direction += randf_range(-PI / 4, PI / 4)
        mob.rotation = direction

        var velocity = Vector2(randf_range(150.0, 250.0), 0.0)
        mob.linear_velocity = velocity.rotated(direction)

        add_child(mob)
